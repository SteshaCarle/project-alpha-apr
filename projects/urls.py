from django.urls import path
from projects.views import list_projects, project_view, create_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", project_view, name="show_project"),
    path("create/", create_project, name="create_project"),
]
