from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)


@login_required
def project_view(request, id):
    project_detail = get_object_or_404(Project, id=id)
    tasks = project_detail.tasks.all()
    context = {
        "show_project": project_detail,
        "tasks": tasks,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = ProjectForm

    context = {"form": form}
    return render(request, "projects/create_project.html", context)
